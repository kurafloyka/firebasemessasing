package com.example.firebasemessaging;

public class MesajModel {

    private String from, message;


    public MesajModel(String from, String message) {
        this.from = from;
        this.message = message;
    }


    public MesajModel() {
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
