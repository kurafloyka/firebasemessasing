package com.example.firebasemessaging;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {


    DatabaseReference reference;
    String userId, otherId, key, user, other;
    List<MesajModel> list;
    RecyclerView recyclerView;
    MesajAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        list = new ArrayList<>();
        recyclerView = findViewById(R.id.listview);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(manager);

        adapter = new MesajAdapter(list, MainActivity.this);
        recyclerView.setAdapter(adapter);


        reference = FirebaseDatabase.getInstance().getReference();
        userId = "1";
        otherId = "2";
        sendMessage("Evdeyim", userId);
        sendMessage("Hello ", otherId);
        sendMessage("Gittin There", userId);
        sendMessage("Saat Kac There", otherId);
        sendMessage("Turkcell There", otherId);
        load();

    }

    public void sendMessage(String messageBody, String id) {

        user = "messages/" + userId + "/" + otherId;
        other = "messages/" + otherId + "/" + userId;
        key = reference.child("messages").child(user).push().getKey();

//userId yap
        Map map = addMessage(messageBody, id);
        Map map1 = new HashMap();
        map1.put(user + "/" + key, map);
        map1.put(other + "/" + key, map);

        reference.updateChildren(map1, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

            }
        });
    }

    public Map addMessage(String messageBody, String userId) {
        Map msg = new HashMap();
        msg.put("message", messageBody);
        msg.put("from", userId);

        return msg;
    }

    public void load() {


        reference.child("messages").child(userId).child(otherId).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                MesajModel m = dataSnapshot.getValue(MesajModel.class);
                list.add(m);
                adapter.notifyDataSetChanged();


            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
